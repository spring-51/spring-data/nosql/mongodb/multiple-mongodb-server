# this app which is connected to multiple mongo server

### References
```
- https://www.applozic.com/blog/configure-multiple-mongodb-connectors-in-spring-boot
```

### Steps

```
S1 - add db details in config files
   - refer application.yml 

S2 - Define bean on MongoTemplate using each mongo uri from config file.
   -  refer MultipleMongoConfig.java
   
S3 - There are 'n' beans of MongoTemplate defined in MultipleMongoConfig, using those n bean create n config classes
   - these config class defines which repo interface connects to which mongo i.e. connect as per base packages
   - refer - PrimaryMongoDBRepositoryConfig
   - refer - SecondaryMongoDBRepositoryConfig 
   
S4 - Exclude auto mongo congfiguration
   - refer - MultipleMongodbServerApplication.java
```