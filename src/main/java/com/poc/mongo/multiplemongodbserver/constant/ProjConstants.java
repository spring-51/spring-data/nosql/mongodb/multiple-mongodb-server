package com.poc.mongo.multiplemongodbserver.constant;

public class ProjConstants {
    public final static String PRIMARY_DB_TEMPLATE_BEAN = "primaryMongoTemplateName";
    public final static String SECONDARY_DB_TEMPLATE_BEAN = "secMongoTemplateName";

    public final static String PRIMARY_DB_BASE_PKG = "com.poc.mongo.multiplemongodbserver.repo.primary.repo";

    public final static String SECONDARY_DB_BASE_PKG = "com.poc.mongo.multiplemongodbserver.repo.secondary.repo";
}
