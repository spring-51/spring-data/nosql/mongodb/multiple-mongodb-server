package com.poc.mongo.multiplemongodbserver.controller;

import com.poc.mongo.multiplemongodbserver.document.Primary;
import com.poc.mongo.multiplemongodbserver.document.Secondary;
import com.poc.mongo.multiplemongodbserver.repo.primary.repo.PrimaryRepository;
import com.poc.mongo.multiplemongodbserver.repo.secondary.repo.SecondaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController {
    @Autowired
    private PrimaryRepository primaryRepository;

    @Autowired
    private SecondaryRepository secondaryRepository;

    @GetMapping(value = "primary")
    public Primary primary(
            @RequestParam(
                    value = "msg",
                    required = false,
                    defaultValue = "Primary message") String msg){
        Primary request = Primary.builder().primaryMsg(msg).build();
        return primaryRepository.save(request);
    }

    @GetMapping(value = "secondary")
    public Secondary secondary(
            @RequestParam(
            value = "msg",
            required = false,
            defaultValue = "Secondary message") String msg){
        Secondary request = Secondary.builder().secondaryMsg(msg).build();
        return secondaryRepository.save(request);
    }
}
