package com.poc.mongo.multiplemongodbserver.document;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "primary")
public class Primary {
    @Id
    private String id;

    private String primaryMsg;
}
