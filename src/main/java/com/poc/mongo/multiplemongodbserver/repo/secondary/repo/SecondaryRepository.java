package com.poc.mongo.multiplemongodbserver.repo.secondary.repo;

import com.poc.mongo.multiplemongodbserver.document.Secondary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecondaryRepository extends MongoRepository<Secondary, String > {
}
