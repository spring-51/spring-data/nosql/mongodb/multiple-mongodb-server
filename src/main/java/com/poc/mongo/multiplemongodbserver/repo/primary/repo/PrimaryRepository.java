package com.poc.mongo.multiplemongodbserver.repo.primary.repo;

import com.poc.mongo.multiplemongodbserver.document.Primary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrimaryRepository extends MongoRepository<Primary, String> {
}
