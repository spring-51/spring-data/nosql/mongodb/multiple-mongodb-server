package com.poc.mongo.multiplemongodbserver.config;

import com.poc.mongo.multiplemongodbserver.constant.ProjConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MultipleMongoConfig {

    @Value(value = "${mongodb.primary.uri}")
    private String primaryDbUri;

    @Value(value = "${mongodb.secondary.uri}")
    private String secondaryDbUri;

    @Bean(name = ProjConstants.PRIMARY_DB_TEMPLATE_BEAN)
    public MongoTemplate primaryMongoTemplate(){
        return new MongoTemplate(primaryMongoDatabaseFactory());
    }
    @Bean(name = ProjConstants.SECONDARY_DB_TEMPLATE_BEAN)
    public MongoTemplate secondaryMongoTemplate(){
        return new MongoTemplate(secondaryMongoDatabaseFactory());
    }

    private MongoDatabaseFactory primaryMongoDatabaseFactory(){
        return new SimpleMongoClientDatabaseFactory(primaryDbUri);
    }

    private MongoDatabaseFactory secondaryMongoDatabaseFactory(){
        return new SimpleMongoClientDatabaseFactory(secondaryDbUri);
    }
}
