package com.poc.mongo.multiplemongodbserver.config;

import com.poc.mongo.multiplemongodbserver.constant.ProjConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(
        basePackages = ProjConstants.SECONDARY_DB_BASE_PKG,
        mongoTemplateRef = ProjConstants.SECONDARY_DB_TEMPLATE_BEAN
)
public class SecondaryMongoDBRepositoryConfig {
}
